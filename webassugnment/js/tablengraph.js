

function tAdd(){
    if(document.getElementById("categories").value != "" && document.getElementById("amount").value!= "" && document.getElementById("date").value!= ""){
        var table = document.getElementById("mytable");
        var row = table.insertRow(1);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);
        var cell5 = row.insertCell(4);

        //create a button element
        var button = document.createElement("Button");
        var t = document.createTextNode("Remove");
        button.classList.add("remove");
        button.appendChild(t);
        var button1 = document.createElement("Button");
        var t = document.createTextNode("Remove");
        button1.classList.add("remove");
        button1.appendChild(t);
        //

        cell1.innerHTML = document.getElementById("categories").value;
        cell2.innerHTML = document.getElementById("date").value;
        cell3.innerHTML = "RM" + document.getElementById("amount").value;
        cell4.appendChild(button);
        cell5.appendChild(button1);

        var index, table = document.getElementById('mytable');
        for(var i = 1; i < table.rows.length; i++) {
            table.rows[i].cells[3].onclick = function() {
                var c = confirm("Do you want to delete this entry?");
                if(c === true) {
                    index = this.parentElement.rowIndex;
                    table.deleteRow(index);
                }
            };
        }
    }else{
        window.alert("Please enter required information");
    }

}

google.charts.load('current', {packages: ['corechart']});
google.charts.setOnLoadCallback(drawDaily);

function drawDaily() {

      var data = google.visualization.arrayToDataTable([
        ['Category', 'Amount(RM)',],
        ['Food', 500],
        ['Social Life', 200],
        ['Transportation', 100],
        ['Education', 50],
        ['Others', 150]
      ]);

      var options = {
        title: 'Daily Expenses',
      };

      var chart = new google.visualization.PieChart(document.getElementById('daily_chart'));

      chart.draw(data, options);
}

google.charts.load('current', {packages: ['corechart','bar']});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {

      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Month');
      data.addColumn('number', 'Amount(RM)');

      data.addRows([
        [{f: 'January'}, 1234],
        [{f: 'February'}, 234],
        [{f:'March'}, 345],
        [{f: 'April'}, 456],
        [{f: 'May'}, 578],
        [{f: 'June'}, 623],
        [{f: 'July'}, 712],
        [{f: 'August'}, 812],
        [{f: 'September'}, 923],
        [{f: 'October'}, 1023],
        [{f: 'November'}, 1023],
        [{f: 'December'}, 1012]
      ]);

      var options = {
        title: 'Daily Expenses',
        hAxis: {
          title: 'Month',
          viewWindow: {
            min: [1, 30, 0],
            max: [12, 30, 0]
          }
        },
        vAxis: {
          title: 'Amount(RM)'
        }
      };

      var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));

      chart.draw(data, options);
}